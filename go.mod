module go-api

go 1.15

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/badoux/checkmail v1.2.1
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/dimiro1/health v0.0.0-20191019130555-c5cbb4d46ffc
	github.com/go-chi/chi v1.5.1
	github.com/go-chi/cors v1.1.1
	github.com/golang-migrate/migrate/v4 v4.14.1
	github.com/google/go-cmp v0.5.4 // indirect
	github.com/google/uuid v1.1.4
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.9.0
	github.com/stretchr/objx v0.3.0 // indirect
	github.com/stretchr/testify v1.6.1
	github.com/vektra/mockery v1.1.2 // indirect
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
