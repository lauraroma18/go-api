package infrastructure

import (
	"github.com/go-chi/chi"
	userApp "go-api/domain/user/application"
	"go-api/infrastructure/auth"
	"go-api/infrastructure/database"
	"net/http"
)

// Routes returns the API Handler with configuration.
func RoutesLogin(conn *database.Data) http.Handler {
	router := chi.NewRouter()

	tk := auth.NewToken()
	lr := userApp.NewLoginHandler(conn, tk)
	router.Mount("/", routesLogin(lr))

	return router
}

// routesFood returns login router with each endpoint.
func routesLogin(handler *userApp.LoginRouter) http.Handler {
	router := chi.NewRouter()

	router.Post("/login", handler.LoginHandler)
	router.Post("/logout", handler.LogoutHandler)
	router.Post("/refresh", handler.RefreshHandler)

	return router
}